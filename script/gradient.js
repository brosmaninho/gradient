var BASE = 16;
var ends = [];
var step = [];
var palette = [];
var gabarito = [];

function Color(r,g,b) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.coll = [r,g,b];
    this.text = colorParseRBG(this.coll);
    this.bg = colorParseRBG(this.coll);
}

function colorParseRBG(rbg) {
    var result = '';
    var m = 1;

    for (var i = 0; i < 3; i++) {
        val = Math.round(rbg[i]/m);
        piece = val.toString(BASE);
        if (piece.length < 2) {piece = '0' + piece;}
        result += piece;
    }

    return '#' + result.toUpperCase();
}

function colorParseHex(hex) {
    var m = 1;
    var color = hex.replace(/[\#rgb\(]*/i,'').toUpperCase();
    if (color.length == 3) {
        var a = color.substr(0,1);
        var b = color.substr(1,1);
        var c = color.substr(2,1);
        color = a + a + b + b + c + c;
    }
    var num = [color.substr(0,2),color.substr(2,2),color.substr(4,2)];

    return [parseInt(num[0],BASE)*m,parseInt(num[1],BASE)*m,parseInt(num[2],BASE)*m];
}

function stepCalc(steps) {
    step[0] = (ends[1].r - ends[0].r) / steps;
    step[1] = (ends[1].g - ends[0].g) / steps;
    step[2] = (ends[1].b - ends[0].b) / steps;
}

function mixPalette(steps) {
    var count = steps + 1;
    palette[0] = new Color(ends[0].r,ends[0].g,ends[0].b);
    palette[count] = new Color(ends[1].r,ends[1].g,ends[1].b);
    for (var i = 1; i < count; i++) {
        var r = (ends[0].r + (step[0] * i));
        var g = (ends[0].g + (step[1] * i));
        var b = (ends[0].b + (step[2] * i));
        palette[i] = new Color(r,g,b);
    }
}

function randOrd() {
    return (Math.round(Math.random())-0.5);
}

function drawPalette(steps,colorOrigin,colorDestiny) {
    
    //Converting from Hex to RBG to enable the steps calculation
    var initialColor = colorParseHex(colorOrigin);    
    ends[0] = new Color(initialColor[0],initialColor[1],initialColor[2]);
    var endColor = colorParseHex(colorDestiny);
    ends[1] = new Color(endColor[0],endColor[1],endColor[2]);
    
    //Calculating the steps between colors
    stepCalc(steps);

    //Filling the steps
    mixPalette(steps);
    var widgets = [];
    for (var i = 0; i < steps + 1; i++) {        
        var html = '<li id="dvColor_'+ i +'" class="new" style="background:' + palette[i].bg + '"></li>';
        if(i == 0){
            html = '<li id="dvColor_'+ i +'" class="new" style="border:5px dashed #EEEEEE; background:' + palette[i].bg + '"></li>';
        }
        widgets.push(html);
        gabarito.push('dvColor_'+ i);
    }    

    //Randomizing the widgets list
    widgets.sort(randOrd);    
    for (var i = 0; i < widgets.length + 1; i++) {                
        gridster.add_widget(widgets[i], 1, 1);        
    }        
}

function verifyGabarito(){
    var listWidgets=document.getElementsByTagName("ul")[0].getElementsByTagName("li");
    var flag = false;
    for (var i = 0; i < listWidgets.length -1; i++) {
        var widget = listWidgets[i];
        var position = parseInt(widget.getAttribute("data-col"));
        if (gabarito[position - 1] === widget.id){
            flag = true
        }else{
            return false;
        }
    }

    return flag;
}